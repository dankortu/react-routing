import React, { Component, Fragment } from 'react';
import { AuthHOC } from 'components/AuthorizeProvider';
import {Redirect} from 'react-router-dom'
class Login extends Component {
  state = {
    login:'',
    password:'',
    error: false,
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleClick = () => {
    const {authorizeUser} = this.props;
    const {login, password} = this.state;

    this.setState({
       error: !authorizeUser(login, password)
    })
 };

  render() {
    const { 
      props: { 
        isAuthorized 
      }, 
      state: { 
        login, password, error
      },
      handleChange,
      handleClick
    } = this;

    return (
      isAuthorized ? 
      (
        <Redirect to="/private" />
      ) : (
        <div className="login">
          <input type="text" name="login" value={login} onChange={handleChange}/>
          <input type="password" name="password" value={password} onChange={handleChange}/>

          {error ? <p>Неправильный логин или пароль</p> : null}

          <button onClick={handleClick}>Войти</button>
        </div>
      )
    )
    
  }
}

export default AuthHOC(Login);
