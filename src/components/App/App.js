import React, { PureComponent } from 'react';
import { Switch, withRouter, Route, Link, Redirect } from 'react-router-dom';
import { AuthorizeProvider } from 'components/AuthorizeProvider';

import Public from 'components/Public'
import PrivateRoute from 'components/PrivateRoute';
import Login from '../Login/Login'

 
export class App extends PureComponent {
  render() {
    return (
      <AuthorizeProvider>
        <div>
          <Link to="/">Public</Link>
          <Link to="/private">Private</Link>
          <Link to="/login">Login</Link>
          
          <Switch>
            <Route exact path="/" component={Public}/>
            <Route exact path="/private" component={PrivateRoute}/>
            <Route exact path="/login" component={Login}/>
            <Redirect from="*" to="/"/>
          </Switch>
        </div>
      </AuthorizeProvider>
    );
  }
}

// это важно!
// необходимо использовать этот хок(withRouter), потому что при использовании нескольких контекстов
// реакт-роутер теряет свой контекст. Причина — использование старого апи.
export default withRouter(App);
